﻿#include "pspautoconnector/findnetwork.h"
#include "pspautoconnector/macaddress.h"
#include "pspautoconnector/time.h"

#include <iwlib.h>
#include <sys/socket.h>
#include <memory>
#include <errno.h>
#include <cstring>
#include <array>
#include <string>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  SCAN_TIMEOUT_USECONDS = pspautoconnector::Time( 10000000 );
    const auto  SCAN_INTERVAL_USECONDS = pspautoconnector::Time( 500000 );

    struct CloseSocket
    {
        void operator()(
            int *   _socketPtr
        ) const
        {
            iw_sockets_close( *_socketPtr );
        }
    };

    typedef std::unique_ptr<
        int
        , CloseSocket
    > SocketCloser;

    struct NetworkInfo
    {
        typedef std::array<
            char
            , IW_ESSID_MAX_SIZE + 1
        > Ssid;

        typedef decltype( iw_point::length ) SsidLength;

        typedef decltype( iwreq_data::mode ) Mode;

        bool        existsAddress;
        sockaddr    address;

        bool        existsSsid;
        Ssid        ssid;
        SsidLength  ssidLength;

        bool    existsMode;
        Mode    mode;

        bool    existsFreq;
        iw_freq freq;

        NetworkInfo(
        )
        {
            this->reset();
        }

        void reset(
        )
        {
            this->existsAddress = false;
            this->existsSsid = false;
            this->existsMode = false;
            this->existsFreq = false;
        }

        bool existsAll(
        ) const
        {
            if( this->existsAddress == false ) {
                return false;
            }

            if( this->existsSsid == false ) {
                return false;
            }

            if( this->existsMode == false ) {
                return false;
            }

            if( this->existsFreq == false ) {
                return false;
            }

            return true;
        }

        void setIwAp(
            const iw_event &    _IW_EVENT
        )
        {
            const auto  IW_AP = _IW_EVENT.u.ap_addr;

            std::memcpy(
                &( this->address )
                , &IW_AP
                , sizeof( this->address )
            );

            this->existsAddress = true;
        }

        void setIwEssid(
            const iw_event &    _IW_EVENT
        )
        {
            const auto &    IW_ESSID = _IW_EVENT.u.essid;

            if( IW_ESSID.length > IW_ESSID_MAX_SIZE ) {
#ifdef  DEBUG
                std::printf( "D:SSIDが長すぎる\n" );
#endif  // DEBUG

                return;
            }

            std::memcpy(
                this->ssid.data()
                , IW_ESSID.pointer
                , IW_ESSID.length
            );
            this->ssid[ IW_ESSID.length ] = '\0';
            this->ssidLength = IW_ESSID.length;

            this->existsSsid = true;
        }

        void setIwMode(
            const iw_event &    _IW_EVENT
        )
        {
            const auto &    IW_MODE = _IW_EVENT.u.mode;

            this->mode = IW_MODE;

            this->existsMode = true;
        }

        void setIwFreq(
            const iw_event &    _IW_EVENT
        )
        {
            const auto &    IW_FREQ = _IW_EVENT.u.freq;

            std::memcpy(
                &( this->freq )
                , &IW_FREQ
                , sizeof( this->freq )
            );

            this->existsFreq = true;
        }
    };

    typedef unsigned short ScanDataSize;

    const auto  SCAN_DATA_BUFFER_SIZE = static_cast< ScanDataSize >( ~0 );

    typedef std::array<
        char
        , SCAN_DATA_BUFFER_SIZE
    > ScanDataBuffer;

    bool getWeVersion(
        decltype( iw_range::we_version_compiled ) & _weVersion
        , const int &                               _SOCKET
        , const std::string &                       _INTERFACE
    )
    {
        auto    iwRange = iw_range();
        if( iw_get_range_info(
            _SOCKET
            , _INTERFACE.c_str()
            , &iwRange
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:レンジ情報取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _weVersion = iwRange.we_version_compiled;

        return true;
    }

    template< typename FUNCTION_T >
    bool iwSetExt(
        const int &             _SOCKET
        , const std::string &   _INTERFACE
        , int                   _request
        , const FUNCTION_T &    _FUNCTION
    )
    {
        auto    iwReq = iwreq();
        std::memset(
            &iwReq
            , 0
            , sizeof( iwReq )
        );

        _FUNCTION( iwReq );

        if( iw_set_ext(
            _SOCKET
            , _INTERFACE.c_str()
            , _request
            , &iwReq
        ) < 0 ) {
#ifdef  DEBUG
            std::printf( "E:iw_set_ext()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool initScanNetwork(
        const int &             _SOCKET
        , const std::string &   _INTERFACE
    )
    {
        return iwSetExt(
            _SOCKET
            , _INTERFACE
            , SIOCSIWSCAN
            , [](
                iwreq & _iwReq
            )
            {
                auto &  data = _iwReq.u.data;

                data.pointer = nullptr;
                data.flags = 0;
                data.length = 0;
            }
        );
    }

    bool getScanData(
        ScanDataBuffer &        _scanDataBuffer
        , ScanDataSize &        _scanDataSize
        , const int &           _SOCKET
        , const std::string &   _INTERFACE
    )
    {
        auto    iwReq = iwreq();
        std::memset(
            &iwReq
            , 0
            , sizeof( iwReq )
        );
        iwReq.u.data.pointer = _scanDataBuffer.data();
        iwReq.u.data.flags = 0;
        iwReq.u.data.length = _scanDataBuffer.size();

        if( pspautoconnector::timeoutProc(
            SCAN_TIMEOUT_USECONDS
            , SCAN_INTERVAL_USECONDS
            , [
                &_SOCKET
                , &_INTERFACE
                , &iwReq
            ]
            (
                bool &  _result
            )
            {
                errno = 0;
                if( iw_get_ext(
                    _SOCKET
                    , _INTERFACE.c_str()
                    , SIOCGIWSCAN
                    , &iwReq
                ) == 0 ) {
                    _result = true;
                    return true;
                }
                if( errno != EAGAIN ) {
#ifdef  DEBUG
                    std::printf( "E:iw_get_ext()に失敗\n" );
#endif  // DEBUG

                    _result = false;
                    return true;
                }

                return false;
            }
        ) == false ) {
            return false;
        }

        const auto &    SCAN_DATA_SIZE = iwReq.u.data.length;

        _scanDataSize = SCAN_DATA_SIZE;

        return true;
    }

    bool setAddress(
        const int &                                 _SOCKET
        , const std::string &                       _INTERFACE
        , const decltype( NetworkInfo::address ) &  _ADDRESS
    )
    {
        return iwSetExt(
            _SOCKET
            , _INTERFACE
            , SIOCSIWAP
            , [
                &_ADDRESS
            ](
                iwreq & _iwReq
            )
            {
                auto &  apAddr = _iwReq.u.ap_addr;

                std::memcpy(
                    &( apAddr )
                    , &_ADDRESS
                    , sizeof( apAddr )
                );
            }
        );
    }

    bool setSsid(
        const int &                                     _SOCKET
        , const std::string &                           _INTERFACE
        , const decltype( NetworkInfo::ssid ) &         _SSID
        , const decltype( NetworkInfo::ssidLength ) &   _SSID_LENGTH
    )
    {
        return iwSetExt(
            _SOCKET
            , _INTERFACE
            , SIOCSIWESSID
            , [
                &_SSID
                , &_SSID_LENGTH
            ](
                iwreq & _iwReq
            )
            {
                auto &  essid = _iwReq.u.essid;

                essid.pointer = const_cast< char * >( _SSID.data() );
                essid.length = _SSID_LENGTH;
                if( iw_get_kernel_we_version() < 21 ) {
                    essid.length++;
                }
            }
        );
    }

    bool setFreq(
        const int &                             _SOCKET
        , const std::string &                   _INTERFACE
        , const decltype( NetworkInfo::freq ) & _FREQ
    )
    {
        return iwSetExt(
            _SOCKET
            , _INTERFACE
            , SIOCSIWFREQ
            , [
                &_FREQ
            ](
                iwreq & _iwReq
            )
            {
                auto &  freq = _iwReq.u.freq;

                std::memcpy(
                    &freq
                    , &_FREQ
                    , sizeof( freq )
                );
                freq.flags = IW_FREQ_FIXED;
            }
        );
    }

    bool setNetworkInfo(
        const int &             _SOCKET
        , const std::string &   _INTERFACE
        , const NetworkInfo &   _NETWORK_INFO
    )
    {
        if( setAddress(
            _SOCKET
            , _INTERFACE
            , _NETWORK_INFO.address
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:アドレスの設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( setSsid(
            _SOCKET
            , _INTERFACE
            , _NETWORK_INFO.ssid
            , _NETWORK_INFO.ssidLength
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:SSIDの設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( setFreq(
            _SOCKET
            , _INTERFACE
            , _NETWORK_INFO.freq
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:チャンネルの設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool findMacAddress(
        const int &                             _SOCKET
        , const std::string &                   _INTERFACE
        , const NetworkInfo &                   _NETWORK_INFO
        , const pspautoconnector::MacAddress &  _MAC_ADDRESS
        , const pspautoconnector::Time &        _TIMEOUT
    )
    {
        if( _NETWORK_INFO.mode != IW_MODE_ADHOC ) {
#ifdef  DEBUG
            std::printf( "E:アドホックネットワークではない\n" );
#endif  // DEBUG

            return false;
        }

        if( setNetworkInfo(
            _SOCKET
            , _INTERFACE
            , _NETWORK_INFO
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク情報の設定に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( pspautoconnector::findMacAddress(
            _INTERFACE
            , _MAC_ADDRESS
            , _TIMEOUT
        ) == false ) {
            return false;
        }

        return true;
    }
}

namespace pspautoconnector {
    bool findNetwork(
        const std::string &     _INTERFACE
        , const MacAddress &    _MAC_ADDRESS
        , const Time &          _TIMEOUT
    )
    {
        auto    socket = iw_sockets_open();
        if( socket < 0 ) {
#ifdef  DEBUG
            std::printf( "E:ネットワーク検索用ソケット生成に失敗\n" );
#endif  // DEBUG

            return 1;
        }
        auto    socketCloser = SocketCloser( &socket );

        auto    weVersion = decltype( iw_range::we_version_compiled )();
        if( getWeVersion(
            weVersion
            , socket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:wireless extentionsのバージョン取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( initScanNetwork(
            socket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:スキャン開始に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    scanDataBuffer = ScanDataBuffer();
        auto    scanDataSize = ScanDataSize();
        if( getScanData(
            scanDataBuffer
            , scanDataSize
            , socket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:スキャンデータの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    stream = stream_descr();
        iw_init_event_stream(
            &stream
            , scanDataBuffer.data()
            , scanDataSize
        );

        auto    networkInfo = NetworkInfo();

        auto    iwEvent = iw_event();
        while( iw_extract_event_stream(
            &stream
            , &iwEvent
            , weVersion
        ) > 0 ) {
            switch( iwEvent.cmd ) {
            case SIOCGIWAP:
                networkInfo.setIwAp( iwEvent );
                break;

            case SIOCGIWMODE:
                networkInfo.setIwMode( iwEvent );
                break;

            case SIOCGIWESSID:
                networkInfo.setIwEssid( iwEvent );
                break;

            case SIOCGIWFREQ:
                networkInfo.setIwFreq( iwEvent );
                break;

            default:
                break;
            }

            if( networkInfo.existsAll() == false ) {
                continue;
            }

            if( findMacAddress(
                socket
                , _INTERFACE
                , networkInfo
                , _MAC_ADDRESS
                , _TIMEOUT
            ) == true ) {
                return true;
            }

            networkInfo.reset();
        }

#ifdef  DEBUG
        std::printf( "D:MACアドレスに対応するPSPが存在しない\n" );
#endif  // DEBUG

        return false;
    }
}

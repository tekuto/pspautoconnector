﻿#include "pspautoconnector/findnetwork.h"
#include "pspautoconnector/macaddress.h"
#include "pspautoconnector/time.h"

#include <string>
#include <iwlib.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    bool checkConnect(
        const std::string &                     _INTERFACE
        , const pspautoconnector::MacAddress &  _MAC_ADDRESS
        , const pspautoconnector::Time &        _TIMEOUT
    )
    {
        if( pspautoconnector::findMacAddress(
            _INTERFACE
            , _MAC_ADDRESS
            , _TIMEOUT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスに対応するPSPを見失った\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }
}

int main(
    int         _argc
    , char **   _argv
)
{
    if( _argc < 6 ) {
#ifdef  DEBUG
        std::printf(
            "使い方: %s 無線LANアダプタ名 PSPのMACアドレス ネットワーク検索インターバル(us) PSP検索タイムアウト(us) PSP接続確認タイムアウト(us)\n"
            , _argv[ 0 ]
        );
#endif  // DEBUG

        return 1;
    }

    const auto  INTERFACE = std::string( _argv[ 1 ] );

    auto    macAddress = pspautoconnector::MacAddress();
    if( pspautoconnector::initMacAddress(
        macAddress
        , _argv[ 2 ]
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:PSPのMACアドレスの初期化に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    auto    intervalFindNetwork = pspautoconnector::Time();
    if( pspautoconnector::strToTime(
        intervalFindNetwork
        , _argv[ 3 ]
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:ネットワーク検索インターバルの初期化に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    auto    timeoutFindNetwork = pspautoconnector::Time();
    if( pspautoconnector::strToTime(
        timeoutFindNetwork
        , _argv[ 4 ]
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:PSP検索タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    auto    timeoutCheckConnect = pspautoconnector::Time();
    if( pspautoconnector::strToTime(
        timeoutCheckConnect
        , _argv[ 5 ]
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:PSP接続確認タイムアウトの初期化に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    while( 1 ) {
        pspautoconnector::intervalProc(
            intervalFindNetwork
            , [
                &INTERFACE
                , &macAddress
                , &timeoutFindNetwork
            ]
            (
                bool &
            )
            {
                if( pspautoconnector::findNetwork(
                    INTERFACE
                    , macAddress
                    , timeoutFindNetwork
                ) == false ) {
#ifdef  DEBUG
                    std::printf( "D:デバイスが存在するネットワークの検索に失敗\n" );
#endif  // DEBUG

                    return false;
                }

                return true;
            }
        );

        while( checkConnect(
            INTERFACE
            , macAddress
            , timeoutCheckConnect
        ) == true );
    }

    return 0;
}

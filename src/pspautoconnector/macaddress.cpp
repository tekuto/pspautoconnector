﻿#include "pspautoconnector/macaddress.h"
#include "pspautoconnector/time.h"

#include <sys/socket.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <utility>
#include <array>
#include <poll.h>
#include <unistd.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  BYTE_STRING_LENGTH = 2;

    const auto  MAC_ADDRESS_ELEMENT_BITS = 8;

    const auto  FIND_INTERVAL_USECONDS = pspautoconnector::Time( 500000 );

    const auto  BUFFER_SIZE = static_cast< unsigned short >( ~0 );

    struct CloseSocket
    {
        void operator()(
            int *   _socketPtr
        ) const
        {
            close( *_socketPtr );
        }
    };

    typedef std::unique_ptr<
        int
        , CloseSocket
    > SocketCloser;

    typedef std::array<
        char
        , BUFFER_SIZE
    > Buffer;

    typedef unsigned short ReadSize;

    const auto  BROADCAST_PACKET = pspautoconnector::MacAddress( { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff } );

    bool strToLl(
        long long &     _ll
        , const char *  _STRING
    )
    {
        auto    endPtr = static_cast< char * >( nullptr );
        auto    ll = std::strtoll(
            _STRING
            , &endPtr
            , 16
        );
        if( endPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }
        if( *endPtr != '\0' ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスに16進数以外の文字が含まれている\n" );
#endif  // DEBUG

            return false;
        }

        _ll = std::move( ll );

        return true;
    }

    void llToMacAddress(
        pspautoconnector::MacAddress &  _macAddress
        , long long                     _ll
    )
    {
        std::for_each(
            _macAddress.rbegin()
            , _macAddress.rend()
            , [
                &_ll
            ]
            (
                pspautoconnector::MacAddress::value_type &  _element
            )
            {
                _element = _ll;
                _ll >>= MAC_ADDRESS_ELEMENT_BITS;
            }
        );
    }

    int newSocket(
    )
    {
        return ::socket(
            AF_PACKET
            , SOCK_RAW
            , htons( ETH_P_ALL )
        );
    }

    bool getInterfaceIndex(
        decltype( sockaddr_ll::sll_ifindex ) &  _interfaceIndex
        , const int &                           _SOCKET
        , const std::string &                   _INTERFACE
    )
    {
        auto    ifReq = ifreq();
        std::memset(
            &ifReq
            , 0
            , sizeof( ifReq )
        );
        std::strncpy(
            ifReq.ifr_name
            , _INTERFACE.c_str()
            , sizeof( ifReq.ifr_name )
        );

        if( ioctl(
            _SOCKET
            , SIOCGIFINDEX
            , &ifReq
        ) < 0 ) {
#ifdef  DEBUG
            std::printf( "E:ioctl()に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _interfaceIndex = ifReq.ifr_ifindex;

        return true;
    }

    bool bindSocket(
        const int &             _SOCKET
        , const std::string &   _INTERFACE
    )
    {
        auto    interfaceIndex = decltype( sockaddr_ll::sll_ifindex )();
        if( getInterfaceIndex(
            interfaceIndex
            , _SOCKET
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:インターフェースのインデックス取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    addr = sockaddr_ll();
        std::memset(
            &addr
            , 0
            , sizeof( addr )
        );
        addr.sll_family = AF_PACKET;
        addr.sll_protocol = htons( ETH_P_ALL );
        addr.sll_ifindex = interfaceIndex;

        if( bind(
            _SOCKET
            , reinterpret_cast< const sockaddr * >( &addr )
            , sizeof( addr )
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:バインドに失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool poll(
        const int & _SOCKET
    )
    {
        auto    pollFd = pollfd();
        std::memset(
            &pollFd
            , 0
            , sizeof( pollFd )
        );
        pollFd.fd = _SOCKET;
        pollFd.events = POLLIN;

        const auto  RESULT = ::poll(
            &pollFd
            , 1
            , 0
        );
        if( RESULT < 0 ) {
#ifdef  DEBUG
            std::printf( "E:poll()でエラー\n" );
#endif  // DEBUG

            return false;
        }
        if( RESULT == 0 ) {
            return false;
        }

        return true;
    }

    bool readData(
        Buffer &        _buffer
        , ReadSize &    _readSize
        , const int &   _SOCKET
    )
    {
        const auto  READ_SIZE = read(
            _SOCKET
            , _buffer.data()
            , _buffer.size()
        );
        if( READ_SIZE < -1 ) {
            return false;
        }

        _readSize = READ_SIZE;

        return true;
    }

    bool isBroadcastPacket(
        const Buffer &      _BUFFER
        , const ReadSize &  _READ_SIZE
    )
    {
        if( _READ_SIZE < BROADCAST_PACKET.size() ) {
#ifdef  DEBUG
            std::printf( "D:パケットサイズが小さすぎる\n" );
#endif  // DEBUG

            return false;
        }

        if( std::memcmp(
            _BUFFER.data()
            , BROADCAST_PACKET.data()
            , BROADCAST_PACKET.size()
        ) != 0 ) {
#ifdef  DEBUG
            std::printf( "D:送信先がブロードキャストではない\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool setMacAddress(
        pspautoconnector::MacAddress &  _macAddress
        , const Buffer &                _BUFFER
        , const ReadSize &              _READ_SIZE
    )
    {
        if( _READ_SIZE < BROADCAST_PACKET.size() + _macAddress.size() ) {
#ifdef  DEBUG
            std::printf( "D:パケットサイズが小さすぎる\n" );
#endif  // DEBUG

            return false;
        }

        std::memcpy(
            _macAddress.data()
            , _BUFFER.data() + BROADCAST_PACKET.size()
            , _macAddress.size()
        );

        return true;
    }

    bool readMacAddress(
        pspautoconnector::MacAddress &  _macAddress
        , const int &                   _SOCKET
    )
    {
        auto    buffer = Buffer();
        auto    readSize = ReadSize();
        if( readData(
            buffer
            , readSize
            , _SOCKET
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:データ読み込みに失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( isBroadcastPacket(
            buffer
            , readSize
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:ブロードキャストパケットではない\n" );
#endif  // DEBUG

            return false;
        }

        auto    macAddress = pspautoconnector::MacAddress();
        if( setMacAddress(
            macAddress
            , buffer
            , readSize
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:MACアドレスの取得に失敗\n" );
#endif  // DEBUG

            return false;
        }

        _macAddress = std::move( macAddress );

        return true;
    }

    bool checkMacAddress(
        const int &                             _SOCKET
        , const pspautoconnector::MacAddress &  _MAC_ADDRESS
    )
    {
        while( true ) {
            if( poll( _SOCKET ) == false ) {
                return false;
            }

            auto    macAddress = pspautoconnector::MacAddress();
            if( readMacAddress(
                macAddress
                , _SOCKET
            ) == false ) {
                continue;
            }

            if( macAddress == _MAC_ADDRESS ) {
                break;
            }
        }

        return true;
    }
}

namespace pspautoconnector {
    bool initMacAddress(
        MacAddress &    _macAddress
        , const char *  _STRING
    )
    {
        if( std::strlen( _STRING ) != MAC_ADDRESS_LENGTH * BYTE_STRING_LENGTH ) {
#ifdef  DEBUG
            std::printf( "E:文字列の長さがMACアドレスの長さではない\n" );
#endif  // DEBUG

            return false;
        }

        long long   macAddressLl;
        if( strToLl(
            macAddressLl
            , _STRING
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:MACアドレスの数値変換に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    macAddress = MacAddress();
        llToMacAddress(
            macAddress
            , macAddressLl
        );

        _macAddress = std::move( macAddress );

        return true;
    }

    bool findMacAddress(
        const std::string &     _INTERFACE
        , const MacAddress &    _MAC_ADDRESS
        , const Time &          _TIMEOUT
    )
    {
        auto    socket = newSocket();
        if( socket < 0 ) {
#ifdef  DEBUG
            std::printf( "E:ソケット生成に失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto    socketCloser = SocketCloser( &socket );

        if( bindSocket(
            socket
            , _INTERFACE
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ソケットのバインドに失敗\n" );
#endif  // DEBUG

            return false;
        }

        if( timeoutProc(
            _TIMEOUT
            , FIND_INTERVAL_USECONDS
            , [
                &socket
                , &_MAC_ADDRESS
            ]
            (
                bool &  _result
            )
            {
                if( checkMacAddress(
                    socket
                    , _MAC_ADDRESS
                ) == true ) {
                    _result = true;
                    return true;
                }

                return false;
            }
        ) == false ) {
            return false;
        }

        return true;
    }
}

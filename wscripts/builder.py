# -*- coding: utf-8 -*-

import importlib
import os.path

_MODULE_PATH_SEPARATOR = '.'

def build(
    _context,
    _modulePath,
):
    module = importlib.import_module( 'wscripts' + _MODULE_PATH_SEPARATOR + _modulePath )

    FEATURES = module.getFeatures()

    TARGET = module.getTarget()

    SOURCES = _generateSources( module.getSources() )

    USE = module.getUse()

    LIB = module.getLib()

    STLIB = module.getStlib( _context )

    _context(
        features = FEATURES,
        target = TARGET,
        source = SOURCES,
        use = USE,
        lib = LIB,
        stlib = STLIB,
    )

def _generateSources(
    _sources,
):
    return sorted(
        _generateSourcesMain(
            _sources,
        )
    )

def _generateSourcesMain(
    _sources,
    _parent = None,
):
    result = []

    TYPE = type( _sources )
    if TYPE is dict:
        for key, values in _sources.items():
            parent = key
            if _parent is not None:
                parent = os.path.join(
                    _parent,
                    parent,
                )

            result += _generateSourcesMain(
                values,
                parent,
            )
    elif TYPE is set or TYPE is list:
        for i in _sources:
            result += _generateSourcesMain(
                i,
                _parent,
            )
    else:
        if _parent is not None:
            _sources = os.path.join(
                _parent,
                _sources,
            )

        result.append( _sources )

    return result

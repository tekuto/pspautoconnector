# -*- coding: utf-8 -*-

from . import common

def getDependModules(
):
    return [
    ]

def getFeatures(
):
    return [
        'cxx',
        'cxxprogram',
    ]

def getTarget(
):
    return 'pspautoconnector'

def getSources(
):
    return {
        common.SOURCE_DIR : {
            'pspautoconnector' : [
                'main.cpp',
                'findnetwork.cpp',
                'macaddress.cpp',
                'time.cpp',
            ],
        },
    }

def getUse(
):
    return [
    ]

def getLib(
):
    return [
        'iw',
    ]

def getStlib(
    _context,
):
    return [
    ]

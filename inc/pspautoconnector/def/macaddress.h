﻿#ifndef PSPAUTOCONNECTOR_DEF_MACADDRESS_H
#define PSPAUTOCONNECTOR_DEF_MACADDRESS_H

#include <array>

namespace pspautoconnector {
    const auto  MAC_ADDRESS_LENGTH = 6;

    typedef std::array< unsigned char, MAC_ADDRESS_LENGTH > MacAddress;
}

#endif  // PSPAUTOCONNECTOR_DEF_MACADDRESS_H

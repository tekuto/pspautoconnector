﻿#ifndef PSPAUTOCONNECTOR_DEF_TIME_H
#define PSPAUTOCONNECTOR_DEF_TIME_H

namespace pspautoconnector {
    typedef long long Time;
}

#endif  // PSPAUTOCONNECTOR_DEF_TIME_H

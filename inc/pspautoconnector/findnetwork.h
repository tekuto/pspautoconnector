﻿#ifndef PSPAUTOCONNECTOR_FINDNETWORK_H
#define PSPAUTOCONNECTOR_FINDNETWORK_H

#include "pspautoconnector/def/macaddress.h"
#include "pspautoconnector/def/time.h"

#include <string>

namespace pspautoconnector {
    bool findNetwork(
        const std::string &
        , const MacAddress &
        , const Time &
    );
}

#endif  // PSPAUTOCONNECTOR_FINDNETWORK_H

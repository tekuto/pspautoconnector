﻿#ifndef PSPAUTOCONNECTOR_MACADDRESS_H
#define PSPAUTOCONNECTOR_MACADDRESS_H

#include "pspautoconnector/def/macaddress.h"
#include "pspautoconnector/def/time.h"

#include <string>

namespace pspautoconnector {
    bool initMacAddress(
        MacAddress &
        , const char *
    );

    bool findMacAddress(
        const std::string &
        , const MacAddress &
        , const Time &
    );
}

#endif  // PSPAUTOCONNECTOR_MACADDRESS_H

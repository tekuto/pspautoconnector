﻿#ifndef PSPAUTOCONNECTOR_TIME_H
#define PSPAUTOCONNECTOR_TIME_H

#include "pspautoconnector/def/time.h"

#include <functional>

namespace pspautoconnector {
    bool strToTime(
        Time &
        , const char *
    );

    bool intervalProc(
        const Time &
        , const std::function< bool( bool & ) > &
    );

    bool timeoutProc(
        const Time &
        , const Time &
        , const std::function< bool( bool & ) > &
    );
}

#endif  // PSPAUTOCONNECTOR_TIME_H
